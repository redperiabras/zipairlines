SHELL := /bin/bash

.PHONY:
	clean
	build
	run
	test

clean:
	find . -iname "*.pyc" | xargs rm -Rf
	find . -iname "*.pyo" | xargs rm -Rf
	find . -iname "*.pyd" | xargs rm -Rf
	find . -iname "__pycache__" | xargs rm -Rf

build:
	make clean
	pip install coverage
	pip install pipenv --upgrade
	pipenv install
	pipenv run ./manage.py migrate

run:
	pipenv run ./manage.py runserver

test:
	coverage run --source='.' manage.py test
	coverage report
