# ZipAirlines - ZipHR Backend Developer Task
A working digital version of the aircraft passenger capacity issue

[![coverage report](https://gitlab.com/redperiabras/zipairlines/badges/master/coverage.svg)](https://gitlab.com/redperiabras/zipairlines/-/commits/master)

## Installation
Run the following command to setup the environment and run the app.

```sh
make build
make run
```

## Usage

A swagger documentation is also available upon running the app.

![Swagger](documentation/swagger.png)

## Tests

To run the tests, simply execute the following command:

```sh
make test
```

## Support
Questions, concerns, issues? Drop me an email [here](mailto:incoming+redperiabras-zipairlines-21168033-issue-@incoming.gitlab.com).