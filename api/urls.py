from django.urls import path
from django.conf.urls import include
from .v1 import urls as v1_urls

urlpatterns = [
    path("v1/", include(v1_urls.urlpattterns))
]