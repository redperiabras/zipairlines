import json
import random
from django.urls import reverse

from .models import Aircraft

from rest_framework.test import (
    APITestCase,
    APIRequestFactory
)
from rest_framework.status import (
    HTTP_200_OK,
    HTTP_201_CREATED,
    HTTP_204_NO_CONTENT,
    HTTP_403_FORBIDDEN
)

# Create your tests here.
class AircraftCrudTestCase(APITestCase):
    """APITestCase for API Responses"""

    __data = {"id": 1, "passengers": "280"}

    def test_create(self):
        """Test POST Request"""        

        response = self.client.post("/api/v1/aircrafts/", json.dumps(self.__data), content_type="application/json")
        response_data = json.loads(response.content)

        self.assertEqual(response.status_code, HTTP_201_CREATED)
        self.assertEqual(response_data['fuel_capacity'], 200)
        self.assertEqual(response_data['fuel_consumption_per_minute'], '0.5600')
        self.assertEqual(response_data['max_airtime'], '357.14')

    def test_list(self):
        """Test GET/list Request"""     

        Aircraft.objects.create(**self.__data)
        Aircraft.objects.create(**{"id": 2, "passengers": 2000})

        response = self.client.get("/api/v1/aircrafts/", content_type="application/json")

        self.assertEqual(response.status_code, HTTP_200_OK)

    def test_retrieve(self):
        """Test GET/retrieve Requests"""   

        Aircraft.objects.create(**self.__data)
        response = self.client.get("/api/v1/aircrafts/1/", content_type="application/json")

        self.assertEqual(response.status_code, HTTP_200_OK)

    def test_partial_update(self):
        """Test PATCH Requests"""   

        Aircraft.objects.create(**self.__data)

        partial_data = {"passengers": "600"}

        response = self.client.patch("/api/v1/aircrafts/1/", json.dumps(partial_data), content_type="application/json")
        response_data = json.loads(response.content)

        self.assertEqual(response.status_code, HTTP_200_OK)
        self.assertEqual(response_data['passengers'], 600)


    def test_delete(self):
        """Test DELETE Requests"""   
        Aircraft.objects.create(**self.__data)

        response = self.client.delete("/api/v1/aircrafts/1/")
        
        self.assertEqual(response.status_code, HTTP_204_NO_CONTENT)


class AircraftLimitTestCase(APITestCase):

    __data = {"id": 1, "passengers": "10"}

    def test_limit_guard(self):
        """Test if POST request was able to check if maximum number of aircraft is already reached"""    

        # Simulate first 10 Inputs
        for i in range(2, 12):
            data = {"id": i, "passengers": random.randint(10, 1000)}
            Aircraft.objects.create(**data)
        
        # Add another input
        response = self.client.post("/api/v1/aircrafts/", json.dumps(self.__data), content_type="application/json")
        
        self.assertEqual(response.status_code, HTTP_403_FORBIDDEN)
