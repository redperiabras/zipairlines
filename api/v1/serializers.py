from rest_framework.serializers import (
    ModelSerializer,
    IntegerField,
    DecimalField
)
from ..models import Aircraft

_MIN_DECIMAL_PLACE = 2
_MAX_DECIMAL_PLACE = 4
_MAX_DIGITS = 10

class AircraftSerializer(ModelSerializer):
    """Serializer class for Aircrafts"""

    class Meta:
        model = Aircraft
        fields = (
            'id',
            'passengers',
            'fuel_capacity',
            'fuel_consumption_per_minute',
            'max_airtime'
        )

    fuel_consumption_per_minute = DecimalField(max_digits=_MAX_DIGITS, 
                                               decimal_places=_MAX_DECIMAL_PLACE, 
                                               read_only=True, help_text="Fuel consumption per minute")
    max_airtime = DecimalField(max_digits=_MAX_DIGITS, decimal_places=_MIN_DECIMAL_PLACE, 
                               read_only=True, help_text="Maximum Airtime in minutes")

