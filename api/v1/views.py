from .serializers import AircraftSerializer
from ..models import Aircraft
from .exceptions import MaxLimitReached

from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response
from rest_framework.status import (
    HTTP_200_OK,
    HTTP_403_FORBIDDEN
)

_MAX_NUMBER_OF_AIRCRAFTS = 10

class AircraftView(ModelViewSet):
    """Viewset to handle and save data"""

    queryset = Aircraft.objects.all()
    serializer_class = AircraftSerializer
    http_method_names = ['get', 'post', 'patch', 'delete', 'head', 'options']

    def create(self, request) -> Response:
        """This method overrides default implementation to check the total count of accommodated requests

        Args:
            request (rest_framework.request.Request): Handles client request

        Raises:
            MaxLimitReached: Raised when max number of aircrafts evaluated is reached

        Returns:
            Response: Handles server response
        """            

        # Guard number of aircrafts
        if self.queryset.count() < _MAX_NUMBER_OF_AIRCRAFTS:
            return super(AircraftView, self).create(request)
        
        raise MaxLimitReached
        