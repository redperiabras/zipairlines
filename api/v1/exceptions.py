from rest_framework.exceptions import APIException

class MaxLimitReached(APIException):
    """API Exception to guard maximum number of aircrafts"""    

    status_code = 403
    default_detail = "Reached maximum number of Aircrafts"
    default_code = 'max_limit'