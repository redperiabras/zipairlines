from .views import AircraftView

from django.conf.urls import url, include
from django.urls import path
from rest_framework.routers import SimpleRouter

from rest_framework import permissions
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

router = SimpleRouter()
router.register(r"aircrafts", AircraftView, basename='aircrafts')

schema_view = get_schema_view(
    openapi.Info(title="ZipAirlines API", default_version="v1", description="",),
    public=True,
    permission_classes=(permissions.AllowAny,),
)

urlpattterns = [
    url(r"", include(router.urls)),
    path(
        "swagger/",
        schema_view.with_ui("swagger", cache_timeout=0),
        name="schema-swagger-ui",
    ),
]