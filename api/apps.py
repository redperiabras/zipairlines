from django.apps import AppConfig


class ApiConfig(AppConfig):
    """Django Application Configuration

    Args:
        AppConfig (django.apps.AppConfig): Django Apps initialization class
    """    

    name = 'api'
