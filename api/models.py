import math

from django.db.models import (
    Model,
    IntegerField,
)

# Constants
_FUEL_CAPACITY_PER_ID = 200
_FUEL_CONSUMPTION_PER_MINUTE = 0.8
_ADDITIONAL_FUEL_CONSUMPTION_PER_PASSENGER_PER_MINUTE = 0.002


class Aircraft(Model):
    """Aircraft database model to handle static data and compute for dynamic data"""    

    id = IntegerField(primary_key=True, help_text="Aircraft ID")
    passengers = IntegerField(default=0, help_text="Number of Passengers")

    @property
    def fuel_capacity(self) -> int:
        """Computes for the fuel capacity as:
            fuel_capacity = i * 200
                where i = Aircraft ID

        Returns:
            int: Fuel capacity
        """        
    
        return self.id * _FUEL_CAPACITY_PER_ID

    @property
    def fuel_consumption_per_minute(self) -> float:
        """Computes for the fuel consumption per minute as:
            fuel_consumption_per_minute = (log(i) * .8) + (y * .002)
                where   i = Aircraft ID; and
                        y = number of passengers

        Returns:
            float: Fuel consumption per minute
        """        

        addtn_per_passenger = self.passengers * _ADDITIONAL_FUEL_CONSUMPTION_PER_PASSENGER_PER_MINUTE

        return (math.log(self.id) * _FUEL_CONSUMPTION_PER_MINUTE) + addtn_per_passenger

    @property
    def max_airtime(self) -> float:
        """Computes for the maximum airtime (in minutes) of the aircraft as:
            max_airtime = x / y
                where   x = Fuel capacity
                        y = Fuel consumption per minute


        Returns:
            float: Maximum Airtime
        """        

        return self.fuel_capacity / self.fuel_consumption_per_minute
